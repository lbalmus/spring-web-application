create table student(
    id int AUTO_INCREMENT PRIMARY KEY,
    uuid uuid DEFAULT random_uuid(),
    name varchar(255),
    age int,
    phone varchar(13),
    gender char(1)
);

INSERT INTO student(name, age, phone, gender) values('Balmus Laurentiu', 30, '0040123456789', 'M');
INSERT INTO student(name, age, phone, gender) values('Balmus Robert', 30, '0040122226789', 'M');
INSERT INTO student(name, age, phone, gender) values('Balmus Amalia', 2, '0040123456789', 'F');
INSERT INTO student(name, age, phone, gender) values('Balmus Maria', 3, '0040122456789', 'F');
INSERT INTO student(name, age, phone, gender) values('Balmus Mihaela', 28, '0040123456789', 'F');