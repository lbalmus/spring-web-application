package ro.deloittece.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import ro.deloittece.web.service.StudentService;

@Controller
public class MainController {

    @Autowired
    private final StudentService studentService;

    public MainController(StudentService studentService) {
        this.studentService = studentService;
    }


    @GetMapping(value = "/index")
    public String findIndexPage(Model model){

        model.addAttribute("students", studentService.findAll());
        return "index";
    }

    @GetMapping
    public String redirectToIndex(){
        return "redirect:/index";
    }

}
