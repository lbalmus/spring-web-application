package ro.deloittece.web.controller;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ro.deloittece.web.dao.StudentDao;
import ro.deloittece.web.service.StudentService;

@Controller
@AllArgsConstructor
public class StudentController {

    @Autowired
    private final StudentService studentService;

    @GetMapping("/student/delete/{uuid}")
    public String deleteStudent(@PathVariable String uuid){
        studentService.deleteStudent(uuid);
        return "redirect:/index";
    }

    @GetMapping("/student/register")
    public String registerStudentPage() {
        return "register";
    }

    @PostMapping("/student/save")
    public String saveStudent(@RequestParam String name,
                              @RequestParam Integer age,
                              @RequestParam String phone,
                              @RequestParam Character gender) {

        studentService.saveStudent(name, age, phone, gender);

        return "redirect:/index";
    }

    @GetMapping("/student/update/{uuid}")
    public String updateStudentPage(@PathVariable String uuid, Model model) {

        StudentDao studentDao = studentService.findByUuid(uuid);
        model.addAttribute("student", studentDao);
        return "update";
    }

    @PostMapping("/student/update")
    public String saveStudent(@RequestParam String uuid,
                              @RequestParam String name,
                              @RequestParam Integer age,
                              @RequestParam String phone,
                              @RequestParam Character gender) {

        studentService.saveStudent(StudentDao.builder()
                        .uuid(uuid)
                        .phone(phone)
                        .name(name)
                        .age(age)
                        .gender(gender)
                        .build());

        return "redirect:/index";
    }
}
