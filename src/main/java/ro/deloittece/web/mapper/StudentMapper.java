package ro.deloittece.web.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import ro.deloittece.web.dao.StudentDao;
import ro.deloittece.web.entity.Student;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StudentMapper {
    public static StudentDao toDto(Student student) {
        return StudentDao.builder()
                .uuid(student.getUuid())
                .age(student.getAge())
                .name(student.getName())
                .gender(student.getGender())
                .phone(student.getPhone())
                .build();
    }
}
