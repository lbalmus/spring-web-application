package ro.deloittece.web.service;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.deloittece.web.dao.StudentDao;
import ro.deloittece.web.entity.Student;
import ro.deloittece.web.mapper.StudentMapper;
import ro.deloittece.web.repository.StudentRepo;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class StudentService {

    @Autowired
    private final StudentRepo studentRepo;

    public List<StudentDao> findAll() {
        return studentRepo.findAll().stream().map(StudentMapper::toDto).collect(Collectors.toList());
    }

    public void deleteStudent(String uuid) {
        studentRepo.delete(studentRepo.findByUuid(uuid));
    }

    public void saveStudent(String name, Integer age, String phone, Character gender) {

        studentRepo.save(Student.builder()
                        .uuid(UUID.randomUUID().toString())
                        .name(name)
                        .age(age)
                        .gender(gender)
                        .phone(phone)
                        .build());
    }

    public void saveStudent(StudentDao studentDao) {

        Student student = studentRepo.findByUuid(studentDao.getUuid());

        student.setAge(studentDao.getAge());
        student.setName(studentDao.getName());
        student.setPhone(studentDao.getPhone());
        student.setGender(studentDao.getGender());

        studentRepo.save(student);
    }

    public StudentDao findByUuid(String uuid) {
        return StudentMapper.toDto(studentRepo.findByUuid(uuid));
    }
}
