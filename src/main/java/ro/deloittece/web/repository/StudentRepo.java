package ro.deloittece.web.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.deloittece.web.entity.Student;

public interface StudentRepo extends JpaRepository<Student, Integer> {
    Student findByUuid(String uuid);
}
