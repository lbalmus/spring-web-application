package ro.deloittece.web.dao;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentDao {
    private String uuid;
    private String name;
    private Integer age;
    private String phone;
    private Character gender;
}
